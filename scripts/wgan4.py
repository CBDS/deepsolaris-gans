# discriminator gives scalar output
# norm visualization it included
# no gradient penalty and no crit repeats

import argparse
import os
from os import listdir
from os.path import isfile, join
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
import random
import math
import warnings
import math
warnings.filterwarnings('ignore')
import cv2
from PIL import Image

import torch
from torch.utils.data import Dataset, DataLoader
from torch import nn
from tqdm import tqdm
from torchvision import transforms
from torchvision.utils import make_grid
from torchvision.utils import save_image
import torch.nn.functional as F
torch.manual_seed(0)

parser = argparse.ArgumentParser()
parser.add_argument('--n_epochs', type=int, default=700, help='number of epochs of training')
parser.add_argument('--batch_size', type=int, default=256, help='size of the batches')
parser.add_argument("--lr", type=float, default=0.0002, help="adam: learning rate")
parser.add_argument("--b1", type=float, default=0.5, help="adam: decay of first order momentum of gradient")
parser.add_argument("--b2", type=float, default=0.999, help="adam: decay of first order momentum of gradient")
parser.add_argument("--z_dim", type=int, default=64, help="dimensionality of the latent space")
parser.add_argument("--csv_file", type=str, default='HR_10000_kmeans_6c.csv', help="name of the csv file")
parser.add_argument("--model_name", type=str, default='unknown', help="name of the model")
parser.add_argument("--img_size", type=int, default=96, help="size of the image")
parser.add_argument("--save_nr", type=int, default=50, help="save the images after this number of epochs")
parser.add_argument("--n_classes", type=int, default=6, help="number of classes in the dataset")
parser.add_argument("--checkpoint_nr", type=int, default=10, help="save model after checkpoint_start + this number of epochs")
parser.add_argument("--checkpoint_start", type=int, default=200, help="start saving the model after this number of epochs")
parser.add_argument("--disc_repeats", type=int, default=5, help="number of times to update disc before updating gen")
parser.add_argument("--gp_weight", type=int, default=10, help="weight of gradient penalty")
args = parser.parse_args()
print(args)

# HYPERPARAMETERS
n_epochs = args.n_epochs
z_dim = args.z_dim
batch_size = args.batch_size
lr = args.lr
b1 = args.b1
b2 = args.b1
model_name = args.model_name
csv_file = args.csv_file
img_size = args.img_size
save_nr = args.save_nr
n_classes = args.n_classes
checkpoint_nr = args.checkpoint_nr
checkpoint_start = args.checkpoint_start
disc_repeats = args.disc_repeats
gp_weight = args.gp_weight

device = 'cuda'
data_shape = (3, img_size, img_size)
cuda = True if torch.cuda.is_available() else False

   
# DEFINE THE PATH FOR RESULTS
result_path = '/data1/Itzel/WGAN_results/' + model_name
# result_path = 'D:/GAN_results/model_' + model_name

loss_plots_path = result_path + '/loss_plots'
gen_imgs_path = result_path + '/gen_imgs'
checkpoints_path = result_path + '/checkpoints'
grad_norms_path = result_path + '/grad_norms'
if not os.path.exists(result_path):
    os.makedirs(result_path)
if not os.path.exists(loss_plots_path):
    os.makedirs(loss_plots_path)
if not os.path.exists(gen_imgs_path):
    os.makedirs(gen_imgs_path)
if not os.path.exists(checkpoints_path):
    os.makedirs(checkpoints_path)
if not os.path.exists(grad_norms_path):
    os.makedirs(grad_norms_path)



# DATASET
class HeerlenDataset(Dataset):
    def __init__(self, csv_file, root_dir, transform=None):
        self.annotations = pd.read_csv(csv_file)
        self.root_dir = root_dir
        self.transform = transform
    
    def __len__(self):
        return len(self.annotations)
    
    def __getitem__(self, index):
        img_path = os.path.join(self.root_dir, self.annotations.iloc[index, 0])
        image = cv2.imread(img_path)
        image =  cv2.cvtColor(image, cv2.COLOR_BGR2RGB)
        image = Image.fromarray(image)
        y_label = torch.tensor(int(self.annotations.iloc[index, 1]))
        
        if self.transform:
            image = self.transform(image)
        
        return (image, y_label)

    

# GET THE DATASET
transform = transforms.Compose([
    transforms.Resize(img_size),
    transforms.ToTensor(),
    # mean = 0.5, std = 0.5, from (0,1) to (-1,1)
    transforms.Normalize((0.5,0.5,0.5), (0.5,0.5,0.5)) 
])

#dataset_path = 'D:/Heerlen_HR_2018/Heerlen_HR_2018/Heerlen_HR_2018/full/'
dataset_path = '/data1/Heerlen_HR_2018/full/'
dataset = HeerlenDataset(csv_file= '/data1/Itzel/csv_files/' + csv_file,
                                      root_dir=dataset_path,
                                      transform=transform)


# GENERATOR
class Generator(nn.Module):

    def __init__(self, im_chan=3, hidden_dim=64):
        super(Generator, self).__init__()
        self.input_dim =  z_dim + n_classes
        # Build the neural network
        self.gen = nn.Sequential(
            self.block(self.input_dim, 256, kernel_size=4, stride=2),
            self.block(256, 128, kernel_size=5, stride=2),
            self.block(128, 64, kernel_size=3, stride=2),
            self.block(64, 32, kernel_size=3, stride=2),
            self.block(32, 3, kernel_size=4, stride=2, final_layer=True),
        )

    def block(self, input_channels, output_channels,kernel_size=3, stride=2, final_layer=False):

        if not final_layer:
            return nn.Sequential(
                nn.ConvTranspose2d(input_channels, output_channels, kernel_size, stride),
                nn.BatchNorm2d(output_channels),
                nn.ReLU(inplace=True),
            )
        else:
            return nn.Sequential(
                nn.ConvTranspose2d(input_channels, output_channels, kernel_size, stride),
                nn.Tanh(),
            )

    def forward(self, noise):

        gen_input = noise.view(len(noise), self.input_dim, 1, 1)
        return self.gen(gen_input)

def get_noise(n_samples, z_dim, device='cpu'):

    return torch.randn(n_samples, z_dim, device=device)


# DISCRIMINATOR
class Discriminator(nn.Module):

    def __init__(self):
        super(Discriminator, self).__init__()
        self.input_dim = data_shape[0] + n_classes 
        self.disc = nn.Sequential(
            self.block(self.input_dim, 64),
            self.block(64, 128),
            self.block(128, 256),
            self.block(256, 512),
            self.block(512, 1, final_layer=True),
        )


    def block(self, input_channels, output_channels, kernel_size=4, stride=2, final_layer=False):

        if not final_layer:
            return nn.Sequential(
                nn.Conv2d(input_channels, output_channels, kernel_size, stride),
                nn.BatchNorm2d(output_channels),
                nn.LeakyReLU(0.2, inplace=True),
            )
        else:
            return nn.Sequential(
                nn.Conv2d(input_channels, output_channels, kernel_size, stride),
            )

    def forward(self, image):

        disc_pred = self.disc(image)
        return disc_pred.view(len(disc_pred), -1)

    

# SOME HELPER FUNCTIONS
def get_one_hot_labels(labels, n_classes):
    return F.one_hot(labels,n_classes)

def combine_vectors(x, y):
    combined = torch.cat([x.float(),y.float()], dim=1)
    return combined

def make_grad_hook():
    gradients = []
    def grad_hook(x):
        if isinstance(x, nn.Conv2d) or isinstance(x, nn.ConvTranspose2d):
            gradients.append(x.weight.grad)
    return gradients, grad_hook



# EVALUATION FUNCTIONS
def show_image(img):
    
    # transform back from (-1,1) to (0,1)
    img = (img + 1) / 2 
    img = img.detach().cpu()
    
    # change from (3,size,size) to (size, size, 3)
    plt.imshow(img.permute(1, 2, 0).squeeze())
    plt.show()


# visualization if 5/6 clusters:
def save_mixed_images(epoch, gen_imgs_path):
    
    nr_images = n_classes*n_classes
    imgs_list = []
    
    for i in range(n_classes):

        # create the one-hot labels per cluster
        label_shape = torch.empty(n_classes)
        labels = label_shape.fill_(i).to(torch.int64)
        one_hot_labels = F.one_hot(labels.to(device),n_classes)
        
        # get the noise per cluster
        noise = get_noise(n_classes, z_dim, device=device)
        noise_and_labels = torch.cat([noise.float(),one_hot_labels.float()],dim=1)
        
        # generate images per noise
        gen_imgs = gen(noise_and_labels)
        imgs_list.append(gen_imgs)

    
    # concatenate the generated images and transform back to original
    if n_classes == 5:
        all_imgs = torch.cat([imgs_list[0], imgs_list[1], imgs_list[2], imgs_list[3], imgs_list[4]], dim=0)
    if n_classes == 6: 
        all_imgs = torch.cat([imgs_list[0], imgs_list[1], imgs_list[2], imgs_list[3], imgs_list[4], imgs_list[5]], dim=0)
    nrow = int(np.sqrt(nr_images))
    epochs_finished = epoch + 2
    save_image(tensor=all_imgs.data,
               fp=gen_imgs_path + '/images_epoch%d.png' % (epochs_finished),
              normalize=True,
               nrow=nrow) 



# visualization if 2 classes
def save_sample_images(nr_images, epoch, gen_imgs_path):

    # create the one-hot positive labels
    label_shape = torch.empty(nr_images) # check if this is the correct label shape
    labels = torch.ones_like(input=label_shape, dtype=torch.int64) # we only want to have positives   
    one_hot_labels = F.one_hot(labels.to(device),n_classes)

    # noise
    noise = get_noise(nr_images, z_dim, device=device)
    noise_and_labels = torch.cat([noise.float(),one_hot_labels.float()],
                                 dim=1)

    # get the generated images and transform back to original 
    gen_imgs = gen(noise_and_labels)
    nrow = int(np.sqrt(nr_images))
    epochs_finished = epoch + 1
    save_image(tensor=gen_imgs.data,
               fp=gen_imgs_path + '/images_epoch%d.png' % (epochs_finished),
              normalize=True,
               nrow=nrow)

    


def plot_losses(generator_losses, discriminator_losses, loss_plots_path):
    fig = plt.figure()
    epochs_finished = epoch + 1
    plt.plot(generator_losses[-100:], label='Generator loss')
    plt.plot(discriminator_losses[-100:], label='Discriminator loss')
    plt.title('Losses')
    plt.xlabel('Last 100 epochs')
    plt.ylabel('Loss')
    plt.legend()
    plt.savefig(loss_plots_path + '/losses_epoch%d.png' % epochs_finished)
    plt.close(fig)


def plot_grad_norms(avg_batch_grad_norms, grad_norms_path):
    fig = plt.figure()
    epochs_finished = epoch + 1
    plt.plot(avg_batch_grad_norms, label='Generator loss')
    plt.title('Average gradient norms of a batch')
    plt.xlabel('Last 1000 iterations')
    plt.ylabel('Loss')
    plt.ylim([-2,2])
    plt.legend()
    plt.savefig(grad_norms_path + '/losses_epoch%d.png' % epochs_finished)
    plt.close(fig)    

# INITIALIZE GEN AND DISC

gen = Generator().to(device)
disc = Discriminator().to(device)


# OPTIMIZERS
opt_G = torch.optim.Adam(gen.parameters(), lr= lr, betas=(b1, b2))
opt_D = torch.optim.Adam(disc.parameters(), lr= lr, betas=(b1, b2))


# INITIALIZE THE WEIGHTS

def weights_init(x):
    # if conv2d or convtranspose2d
    if isinstance(x, nn.Conv2d) or isinstance(x, nn.ConvTranspose2d):
        torch.nn.init.normal_(x.weight, 0.0, 0.02)
    # if batchnorm
    if isinstance(x, nn.BatchNorm2d):
        torch.nn.init.normal_(x.weight, 1.0, 0.02)
        torch.nn.init.constant_(x.bias, 0)

gen = gen.apply(weights_init)
disc = disc.apply(weights_init)



# DATASET LOADER
dataloader = DataLoader(dataset=dataset,
                         batch_size=batch_size,
                         shuffle=True)



# GRADIENT PENALTY

def get_gradient(disc, real, fake, eps):
    mixed_images = real * eps + fake * (1-eps)
    mixed_images_and_labels = combine_vectors(mixed_images, image_one_hot_labels)
    mixed_scores = disc(mixed_images_and_labels)
    grad = torch.autograd.grad(
        inputs = mixed_images,
        outputs = mixed_scores,
        grad_outputs = torch.ones_like(mixed_scores),
        create_graph = True,
        retain_graph = True
    )[0]
    return grad

def gradient_penalty(grad):
    # flatten the gradients
    grad = grad.view(len(grad), -1)
    # compute the norm 
    grad_norm = grad.norm(2, dim=1)
    
    # penalize the mean squared distance of the gradient norms from 1    
    avg_batch_grad_norm = sum(grad_norm)/grad_norm.shape[0]
    penalty = (avg_batch_grad_norm-1)**2
    return penalty, avg_batch_grad_norm


# TRAINING

generator_losses = []
discriminator_losses = []
avg_batch_grad_norms = []
for epoch in range(n_epochs):
    # i is the batch number
    for i, (real, labels) in enumerate(tqdm(dataloader)):
        real = real.to(device)
        
        # get the one-hot labels for the gen and disc
        one_hot_labels = get_one_hot_labels(labels.to(device), n_classes)
        image_one_hot_labels = one_hot_labels[:, :, None, None]
        image_one_hot_labels = image_one_hot_labels.repeat(1, 1, data_shape[1], data_shape[2])
        
        # mean_iteration_disc_loss = 0

        # ====================
        # UPDATE DISCRIMINATOR
        # ====================

        # zero out the gradients
        opt_D.zero_grad()

        # get the noise
        fake_noise = get_noise(len(real), z_dim, device=device)

        # concatenate the noise to the one-hot labels
        noise_and_labels = combine_vectors(fake_noise, one_hot_labels)

        # generate the fakes
        fake = gen(noise_and_labels)

        # concatenate the images to the labels (make sure to detach the fakes)
        fake_image_and_labels = combine_vectors(fake, image_one_hot_labels).detach()
        real_image_and_labels = combine_vectors(real, image_one_hot_labels)

        # get the discriminator predictions
        disc_fake_pred = disc(fake_image_and_labels)
        disc_real_pred = disc(real_image_and_labels)

        # gradient penalty & grad norm computation
        epsilon = torch.rand(len(real), 1, 1, 1, device=device, requires_grad=True)
        grad = get_gradient(disc, real, fake.detach(), epsilon)
        gp, avg_batch_grad_norm = gradient_penalty(grad)

        # disc loss with grad penalty
        disc_loss = -disc_real_pred.mean() + disc_fake_pred.mean() + gp * gp_weight

        # disc loss without grad penalty
        disc_loss = -disc_real_pred.mean() + disc_fake_pred.mean()


        # keep track of the average disc loss in this batch
        
        # mean_iteration_disc_loss += disc_loss.item() / disc_repeats
        avg_batch_grad_norms += [avg_batch_grad_norm.item()]

        # update gradients
        disc_loss.backward(retain_graph=True)

        # update optimizer
        opt_D.step()
            
      
        # ================
        # UPDATE GENERATOR 
        # ================
        
        # zero out the gradients
        opt_G.zero_grad()
        
        # get new noise
        fake_noise_2 = get_noise(len(real), z_dim, device=device)
        noise_and_labels_2 = combine_vectors(fake_noise_2, one_hot_labels)
        fake_2 = gen(noise_and_labels_2)

        # concatenate the fakes to the one-hot img labels
        fake_image_and_labels_2 = combine_vectors(fake_2, image_one_hot_labels)
        
        # get the predictions for the fakes
        disc_fake_pred = disc(fake_image_and_labels_2)
        
        # get gen loss
        gen_loss = -disc_fake_pred.mean()
        
        # backpropagation to compute gradients for all layers
        gen_loss.backward()
        
        # update weights for this batch
        opt_G.step()
        
        # current epoch nr * iter per epochs + iterations in current loop 
        batches_per_epoch = len(dataloader)
        total_finished_batches = (epoch + 1) * (batches_per_epoch) + (i + 1)
        
        # save loss in list after each epoch
        if total_finished_batches % batches_per_epoch == 0:
            # discriminator_losses += [mean_iteration_disc_loss] 
            discriminator_losses += [disc_loss.item()]
            generator_losses += [gen_loss.item()]

        # only save gen/loss progress images every save_nr epochs (if 5000 epochs)
        if total_finished_batches % (batches_per_epoch * save_nr) == 0:
            save_mixed_images(25, epoch, gen_imgs_path)
            plot_losses(generator_losses, discriminator_losses, loss_plots_path)
            plot_grad_norms(avg_batch_grad_norms, grad_norms_path)
        if (epoch + 1) > checkpoint_start and total_finished_batches % (batches_per_epoch * checkpoint_nr) == 0:
            torch.save({
                'G_state_dict': gen.state_dict(),
                'D_state_dict': disc.state_dict(),
                'opt_G': opt_G.state_dict(),
                'opt_D': opt_D.state_dict()
            }, checkpoints_path + '/chkpt_epoch%d.pt' % (epoch + 1))

    # print the numbers after each epoch
    print('[Epoch %d/%d] [D loss: %f] [G loss: %f]' % 
    (epoch+1,  n_epochs,
    discriminator_losses[-1], generator_losses[-1]))




