{
 "cells": [
  {
   "cell_type": "code",
   "execution_count": 101,
   "metadata": {},
   "outputs": [],
   "source": [
    "# same architecture as gan4.py\n",
    "\n",
    "import argparse\n",
    "import os\n",
    "from os import listdir\n",
    "from os.path import isfile, join\n",
    "import numpy as np\n",
    "import pandas as pd\n",
    "import matplotlib.pyplot as plt\n",
    "import random\n",
    "import math\n",
    "import warnings\n",
    "import math\n",
    "warnings.filterwarnings('ignore')\n",
    "import cv2\n",
    "from PIL import Image\n",
    "\n",
    "import torch\n",
    "from torch.utils.data import Dataset, DataLoader\n",
    "from torch import nn\n",
    "from tqdm import tqdm\n",
    "from torchvision import transforms\n",
    "from torchvision.utils import make_grid\n",
    "from torchvision.utils import save_image\n",
    "import torch.nn.functional as F\n",
    "torch.manual_seed(0)\n",
    "\n",
    "# parser = argparse.ArgumentParser()\n",
    "# parser.add_argument('--n_epochs', type=int, default=200, help='number of epochs of training')\n",
    "# parser.add_argument('--batch_size', type=int, default=256, help='size of the batches')\n",
    "# parser.add_argument(\"--lr\", type=float, default=0.0002, help=\"adam: learning rate\")\n",
    "# parser.add_argument(\"--b1\", type=float, default=0.5, help=\"adam: decay of first order momentum of gradient\")\n",
    "# parser.add_argument(\"--b2\", type=float, default=0.999, help=\"adam: decay of first order momentum of gradient\")\n",
    "# parser.add_argument(\"--z_dim\", type=int, default=64, help=\"dimensionality of the latent space\")\n",
    "# parser.add_argument(\"--csv_file\", type=str, default='id_and_label_10000_hclusters.csv', help=\"name of the csv file\")\n",
    "# parser.add_argument(\"--model_name\", type=str, default='base1', help=\"name of the model\")\n",
    "# parser.add_argument(\"--img_size\", type=int, default=96, help=\"size of the image\")\n",
    "# parser.add_argument(\"--save_nr\", type=int, default=50, help=\"save the images after this number of epochs\")\n",
    "# parser.add_argument(\"--n_classes\", type=int, default=5, help=\"number of classes in the dataset\")\n",
    "# parser.add_argument(\"--checkpoint_nr\", type=int, default=10, help=\"save model after checkpoint_start + this number of epochs\")\n",
    "# parser.add_argument(\"--checkpoint_start\", type=int, default=200, help=\"start saving the model after this number of epochs\")\n",
    "# parser.add_argument(\"--disc_repeats\", type=int, default=5, help=\"number of times to update disc before updating gen\")\n",
    "# parser.add_argument(\"--gp_weight\", type=int, default=10, help=\"weight of gradient penalty\")\n",
    "# args = parser.parse_args()\n",
    "# print(args)\n",
    "\n",
    "# HYPERPARAMETERS\n",
    "\n",
    "n_epochs = 2\n",
    "z_dim = 64\n",
    "batch_size = 256\n",
    "lr = 0.0002\n",
    "b1 = 0.5\n",
    "b2 = 0.999\n",
    "model_name = '01_wgantryout_0802'\n",
    "csv_file = 'id_and_label_10000.csv'\n",
    "img_size = 96\n",
    "save_nr = 10\n",
    "n_classes = 6\n",
    "checkpoint_nr = 50\n",
    "checkpoint_start = 200\n",
    "disc_repeats = 5\n",
    "gp_weight = 10\n",
    "\n",
    "# n_epochs = args.n_epochs\n",
    "# z_dim = args.z_dim\n",
    "# batch_size = args.batch_size\n",
    "# lr = args.lr\n",
    "# b1 = args.b1\n",
    "# b2 = args.b1\n",
    "# model_name = args.model_name\n",
    "# csv_file = args.csv_file\n",
    "# img_size = args.img_size\n",
    "# save_nr = args.save_nr\n",
    "# n_classes = args.n_classes\n",
    "# checkpoint_nr = args.checkpoint_nr\n",
    "# checkpoint_start = args.checkpoint_start\n",
    "# disc_repeats = args.disc_repeats\n",
    "# gp_weight = args.gp_weight\n",
    "\n",
    "device = 'cuda'\n",
    "data_shape = (3, img_size, img_size)\n",
    "cuda = True if torch.cuda.is_available() else False\n",
    "\n",
    "\n",
    "# # DEFINE THE PATH FOR RESULTS\n",
    "# result_path = '/data1/Itzel/GAN_results/' + model_name\n",
    "# # result_path = 'D:/GAN_results/model_' + model_name\n",
    "\n",
    "# loss_plots_path = result_path + '/loss_plots'\n",
    "# gen_imgs_path = result_path + '/gen_imgs'\n",
    "# checkpoints_path = result_path + '/checkpoints'\n",
    "# if not os.path.exists(result_path):\n",
    "#     os.makedirs(result_path)\n",
    "# if not os.path.exists(loss_plots_path):\n",
    "#     os.makedirs(loss_plots_path)\n",
    "# if not os.path.exists(gen_imgs_path):\n",
    "#     os.makedirs(gen_imgs_path)\n",
    "# if not os.path.exists(checkpoints_path):\n",
    "#     os.makedirs(checkpoints_path)\n",
    "\n",
    "\n",
    "\n",
    "# DATASET\n",
    "class HeerlenDataset(Dataset):\n",
    "    def __init__(self, csv_file, root_dir, transform=None):\n",
    "        self.annotations = pd.read_csv(csv_file)\n",
    "        self.root_dir = root_dir\n",
    "        self.transform = transform\n",
    "    \n",
    "    def __len__(self):\n",
    "        return len(self.annotations)\n",
    "    \n",
    "    def __getitem__(self, index):\n",
    "        img_path = os.path.join(self.root_dir, self.annotations.iloc[index, 0])\n",
    "        image = cv2.imread(img_path)\n",
    "        image =  cv2.cvtColor(image, cv2.COLOR_BGR2RGB)\n",
    "        image = Image.fromarray(image)\n",
    "        y_label = torch.tensor(int(self.annotations.iloc[index, 1]))\n",
    "        \n",
    "        if self.transform:\n",
    "            image = self.transform(image)\n",
    "        \n",
    "        return (image, y_label)\n",
    "\n",
    "    \n",
    "\n",
    "# GET THE DATASET\n",
    "transform = transforms.Compose([\n",
    "    transforms.Resize(img_size),\n",
    "    transforms.ToTensor(),\n",
    "    # mean = 0.5, std = 0.5, from (0,1) to (-1,1)\n",
    "    transforms.Normalize((0.5,0.5,0.5), (0.5,0.5,0.5)) \n",
    "])\n",
    "\n",
    "#dataset_path = 'D:/Heerlen_HR_2018/Heerlen_HR_2018/Heerlen_HR_2018/full/'\n",
    "dataset_path = '/data1/Heerlen_HR_2018/full/'\n",
    "dataset = HeerlenDataset(csv_file= '/data1/Itzel/csv_files/unused_files/' + csv_file,\n",
    "                                      root_dir=dataset_path,\n",
    "                                      transform=transform)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 35,
   "metadata": {},
   "outputs": [],
   "source": [
    "\n",
    "# GENERATOR\n",
    "class Generator(nn.Module):\n",
    "\n",
    "    def __init__(self, im_chan=3, hidden_dim=64):\n",
    "        super(Generator, self).__init__()\n",
    "        self.input_dim =  z_dim + n_classes\n",
    "        # Build the neural network\n",
    "        self.gen = nn.Sequential(\n",
    "            self.block(self.input_dim, 256, kernel_size=4, stride=2),\n",
    "            self.block(256, 128, kernel_size=5, stride=2),\n",
    "            self.block(128, 64, kernel_size=3, stride=2),\n",
    "            self.block(64, 32, kernel_size=3, stride=2),\n",
    "            self.block(32, 3, kernel_size=4, stride=2, final_layer=True),\n",
    "        )\n",
    "\n",
    "    def block(self, input_channels, output_channels,kernel_size=3, stride=2, final_layer=False):\n",
    "\n",
    "        if not final_layer:\n",
    "            return nn.Sequential(\n",
    "                nn.ConvTranspose2d(input_channels, output_channels, kernel_size, stride),\n",
    "                nn.BatchNorm2d(output_channels),\n",
    "                nn.ReLU(inplace=True),\n",
    "            )\n",
    "        else:\n",
    "            return nn.Sequential(\n",
    "                nn.ConvTranspose2d(input_channels, output_channels, kernel_size, stride),\n",
    "                nn.Tanh(),\n",
    "            )\n",
    "\n",
    "    def forward(self, noise):\n",
    "\n",
    "        gen_input = noise.view(len(noise), self.input_dim, 1, 1)\n",
    "        return self.gen(gen_input)\n",
    "\n",
    "def get_noise(n_samples, z_dim, device='cpu'):\n",
    "\n",
    "    return torch.randn(n_samples, z_dim, device=device)\n",
    "\n",
    "\n",
    "# DISCRIMINATOR\n",
    "# class Discriminator(nn.Module):\n",
    "\n",
    "#     def __init__(self):\n",
    "#         super(Discriminator, self).__init__()\n",
    "#         self.input_dim = data_shape[0] + n_classes \n",
    "#         self.disc = nn.Sequential(\n",
    "#             self.block(self.input_dim, 32),\n",
    "#             self.block(32, 64),\n",
    "#             self.block(64, 128),\n",
    "#             self.block(128, 1, final_layer=True),\n",
    "#         )\n",
    "\n",
    "#     def block(self, input_channels, output_channels, kernel_size=4, stride=2, final_layer=False):\n",
    "\n",
    "#         if not final_layer:\n",
    "#             return nn.Sequential(\n",
    "#                 nn.Conv2d(input_channels, output_channels, kernel_size, stride),\n",
    "#                 nn.BatchNorm2d(output_channels),\n",
    "#                 nn.LeakyReLU(0.2, inplace=True),\n",
    "#             )\n",
    "#         else:\n",
    "#             return nn.Sequential(\n",
    "#                 nn.Conv2d(input_channels, output_channels, kernel_size, stride),\n",
    "#             )\n",
    "\n",
    "#     def forward(self, image):\n",
    "\n",
    "#         disc_pred = self.disc(image)\n",
    "#         return disc_pred.view(len(disc_pred), -1)\n",
    "    \n",
    " \n",
    "\n",
    "    \n",
    "\n",
    "# SOME HELPER FUNCTIONS\n",
    "def get_one_hot_labels(labels, n_classes):\n",
    "    return F.one_hot(labels,n_classes)\n",
    "\n",
    "def combine_vectors(x, y):\n",
    "    combined = torch.cat([x.float(),y.float()], dim=1)\n",
    "    return combined\n",
    "\n",
    "def make_grad_hook():\n",
    "    gradients = []\n",
    "    def grad_hook(x):\n",
    "        if isinstance(x, nn.Conv2d) or isinstance(x, nn.ConvTranspose2d):\n",
    "            gradients.append(x.weight.grad)\n",
    "    return gradients, grad_hook\n",
    "\n",
    "\n",
    "\n",
    "# EVALUATION FUNCTIONS\n",
    "def show_image(img):\n",
    "    \n",
    "    # transform back from (-1,1) to (0,1)\n",
    "    img = (img + 1) / 2 \n",
    "    img = img.detach().cpu()\n",
    "    \n",
    "    # change from (3,size,size) to (size, size, 3)\n",
    "    plt.imshow(img.permute(1, 2, 0).squeeze())\n",
    "    plt.show()\n",
    "    \n",
    "    \n",
    "def save_mixed_images(nr_images, epoch, gen_imgs_path):\n",
    "    \n",
    "    imgs_list = []\n",
    "    \n",
    "    for i in range(5):\n",
    "\n",
    "        # create the one-hot labels per cluster\n",
    "        label_shape = torch.empty(round(nr_images/5))\n",
    "        labels = label_shape.fill_(i).to(torch.int64)\n",
    "        one_hot_labels = F.one_hot(labels.to(device),n_classes)\n",
    "        \n",
    "        # get the noise per cluster\n",
    "        noise = get_noise(round(nr_images/5), z_dim, device=device)\n",
    "        noise_and_labels = torch.cat([noise.float(),one_hot_labels.float()],dim=1)\n",
    "        \n",
    "        # generate images per noise\n",
    "        gen_imgs = gen(noise_and_labels)\n",
    "        imgs_list.append(gen_imgs)\n",
    "\n",
    "    \n",
    "    # concatenate the generated images and transform back to original\n",
    "    all_imgs = torch.cat([imgs_list[0], imgs_list[1], imgs_list[2], imgs_list[3], imgs_list[4]], dim=0)\n",
    "    nrow = int(np.sqrt(nr_images))\n",
    "    epochs_finished = epoch + 2\n",
    "    save_image(tensor=all_imgs.data,\n",
    "               fp=gen_imgs_path + '/images_epoch%d.png' % (epochs_finished),\n",
    "              normalize=True,\n",
    "               nrow=nrow) \n",
    "\n",
    "\n",
    "# visualization if 2 classes\n",
    "# def save_sample_images(nr_images, epoch, gen_imgs_path):\n",
    "    \n",
    "#     # create the one-hot positive labels\n",
    "#     label_shape = torch.empty(nr_images) # check if this is the correct label shape\n",
    "#     labels = torch.ones_like(input=label_shape, dtype=torch.int64) # we only want to have positives   \n",
    "#     one_hot_labels = F.one_hot(labels.to(device),n_classes)\n",
    "\n",
    "#     # noise\n",
    "#     noise = get_noise(nr_images, z_dim, device=device)\n",
    "#     noise_and_labels = torch.cat([noise.float(),one_hot_labels.float()],\n",
    "#                                  dim=1)\n",
    "\n",
    "#     # get the generated images and transform back to original \n",
    "#     gen_imgs = gen(noise_and_labels)\n",
    "#     nrow = int(np.sqrt(nr_images))\n",
    "#     epochs_finished = epoch + 1\n",
    "#     save_image(tensor=gen_imgs.data,\n",
    "#                fp=gen_imgs_path + '/images_epoch%d.png' % (epochs_finished),\n",
    "#               normalize=True,\n",
    "#                nrow=nrow)\n",
    "    \n",
    "    \n",
    "\n",
    "\n",
    "def plot_losses(generator_losses, discriminator_losses, loss_plots_path):\n",
    "    fig = plt.figure()\n",
    "    epochs_finished = epoch + 1\n",
    "    plt.plot(generator_losses[-100:], label='Generator loss')\n",
    "    plt.plot(discriminator_losses[-100:], label='Discriminator loss')\n",
    "    plt.title('Losses')\n",
    "    plt.xlabel('Last 100 epochs')\n",
    "    plt.ylabel('Loss')\n",
    "    plt.legend()\n",
    "    plt.savefig(loss_plots_path + '/losses_epoch%d.png' % epochs_finished)\n",
    "    plt.close(fig)\n",
    "\n",
    "#\n",
    "# INITIALIZE GEN AND DISC\n",
    "\n",
    "gen = Generator().to(device)\n",
    "disc = Discriminator().to(device)\n",
    "\n",
    "\n",
    "# OPTIMIZERS\n",
    "opt_G = torch.optim.Adam(gen.parameters(), lr= lr, betas=(b1, b2))\n",
    "opt_D = torch.optim.Adam(disc.parameters(), lr= lr, betas=(b1, b2))\n",
    "\n",
    "\n",
    "# INITIALIZE THE WEIGHTS\n",
    "\n",
    "def weights_init(x):\n",
    "    # if conv2d or convtranspose2d\n",
    "    if isinstance(x, nn.Conv2d) or isinstance(x, nn.ConvTranspose2d):\n",
    "        torch.nn.init.normal_(x.weight, 0.0, 0.02)\n",
    "    # if batchnorm\n",
    "    if isinstance(x, nn.BatchNorm2d):\n",
    "        torch.nn.init.normal_(x.weight, 1.0, 0.02)\n",
    "        torch.nn.init.constant_(x.bias, 0)\n",
    "\n",
    "gen = gen.apply(weights_init)\n",
    "disc = disc.apply(weights_init)\n",
    "\n",
    "\n",
    "\n",
    "# DATASET LOADER\n",
    "dataloader = DataLoader(dataset=dataset,\n",
    "                         batch_size=batch_size,\n",
    "                         shuffle=True)\n",
    "\n",
    "\n",
    "\n",
    "# GRADIENT PENALTY\n",
    "\n",
    "def get_gradient(disc, real, fake, eps):\n",
    "    mixed_images = real * eps + fake * (1-eps)\n",
    "    mixed_images_and_labels = combine_vectors(mixed_images, image_one_hot_labels)\n",
    "    mixed_scores = disc(mixed_images_and_labels)\n",
    "    grad = torch.autograd.grad(\n",
    "        inputs = mixed_images,\n",
    "        outputs = mixed_scores,\n",
    "        grad_outputs = torch.ones_like(mixed_scores),\n",
    "        create_graph = True,\n",
    "        retain_graph = True\n",
    "    )[0]\n",
    "    return grad\n",
    "\n",
    "def gradient_penalty(grad):\n",
    "    # flatten the gradients\n",
    "    grad = grad.view(len(grad), -1)\n",
    "    # compute the norm \n",
    "    grad_norm = grad.norm(2, dim=1)\n",
    "    # penalize the mean squared distance of the gradient norms from 1\n",
    "    penalty = (sum(grad_norm)/grad_norm.shape[0]-1)**2\n",
    "    return penalty"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 150,
   "metadata": {},
   "outputs": [],
   "source": [
    "# DISCRIMINATOR\n",
    "class Discriminator(nn.Module):\n",
    "\n",
    "    def __init__(self):\n",
    "        super(Discriminator, self).__init__()\n",
    "        self.input_dim = data_shape[0] + n_classes \n",
    "        self.disc = nn.Sequential(\n",
    "            self.block(self.input_dim, 64),\n",
    "            self.block(64, 128),\n",
    "            self.block(128, 256),\n",
    "            self.block(256, 512),\n",
    "            self.block(512,  1, final_layer=True),\n",
    "        )\n",
    "\n",
    "    def block(self, input_channels, output_channels, kernel_size=4, stride=2, final_layer=False):\n",
    "\n",
    "        if not final_layer:\n",
    "            return nn.Sequential(\n",
    "                nn.Conv2d(input_channels, output_channels, kernel_size, stride),\n",
    "                nn.BatchNorm2d(output_channels),\n",
    "                nn.LeakyReLU(0.2, inplace=True),\n",
    "            )\n",
    "        else:\n",
    "            return nn.Sequential(\n",
    "                nn.Conv2d(input_channels, output_channels, kernel_size, stride),\n",
    "            )\n",
    "\n",
    "    def forward(self, image):\n",
    "\n",
    "        disc_pred = self.disc(image)\n",
    "        return disc_pred.view(len(disc_pred), -1)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 151,
   "metadata": {},
   "outputs": [],
   "source": [
    "# INITIALIZE GEN AND DISC\n",
    "\n",
    "gen = Generator().to(device)\n",
    "disc = Discriminator().to(device)\n",
    "\n",
    "\n",
    "# OPTIMIZERS\n",
    "opt_G = torch.optim.Adam(gen.parameters(), lr= lr, betas=(b1, b2))\n",
    "opt_D = torch.optim.Adam(disc.parameters(), lr= lr, betas=(b1, b2))\n",
    "\n",
    "\n",
    "# INITIALIZE THE WEIGHTS\n",
    "\n",
    "def weights_init(x):\n",
    "    # if conv2d or convtranspose2d\n",
    "    if isinstance(x, nn.Conv2d) or isinstance(x, nn.ConvTranspose2d):\n",
    "        torch.nn.init.normal_(x.weight, 0.0, 0.02)\n",
    "    # if batchnorm\n",
    "    if isinstance(x, nn.BatchNorm2d):\n",
    "        torch.nn.init.normal_(x.weight, 1.0, 0.02)\n",
    "        torch.nn.init.constant_(x.bias, 0)\n",
    "\n",
    "gen = gen.apply(weights_init)\n",
    "disc = disc.apply(weights_init)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 152,
   "metadata": {},
   "outputs": [],
   "source": [
    "real0 = dataset[0][0][None,:,:,:]\n",
    "real1 = dataset[1][0][None,:,:,:]\n",
    "real2 = dataset[2][0][None,:,:,:]\n",
    "real3 = dataset[3][0][None,:,:,:]\n",
    "real4 = dataset[4][0][None,:,:,:]\n",
    "real5 = dataset[5][0][None,:,:,:]\n",
    "real6 = dataset[6][0][None,:,:,:]\n",
    "real7 = dataset[7][0][None,:,:,:]"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 153,
   "metadata": {},
   "outputs": [],
   "source": [
    "labels0 = dataset[0][1].reshape(-1)\n",
    "labels1 = dataset[1][1].reshape(-1)\n",
    "labels2 = dataset[2][1].reshape(-1)\n",
    "labels3 = dataset[3][1].reshape(-1)\n",
    "labels4 = dataset[4][1].reshape(-1)\n",
    "labels5 = dataset[5][1].reshape(-1)\n",
    "labels6 = dataset[6][1].reshape(-1)\n",
    "labels7 = dataset[7][1].reshape(-1)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 154,
   "metadata": {},
   "outputs": [],
   "source": [
    "batch_size = 8"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 155,
   "metadata": {},
   "outputs": [],
   "source": [
    "real = torch.cat([real0, real1, real2, real3, real4, real5, real6, real7,], dim=0)\n",
    "labels = torch.cat([labels0, labels1, labels2, labels3, labels4, labels5, labels6, labels7,], dim=0)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 156,
   "metadata": {},
   "outputs": [],
   "source": [
    "real = real.to(device)\n",
    "labels = labels.to(device)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 157,
   "metadata": {},
   "outputs": [],
   "source": [
    "one_hot_labels = get_one_hot_labels(labels, n_classes)\n",
    "image_one_hot_labels = one_hot_labels[:, :, None, None]\n",
    "image_one_hot_labels = image_one_hot_labels.repeat(1, 1, data_shape[1], data_shape[2])"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 158,
   "metadata": {},
   "outputs": [],
   "source": [
    "opt_D.zero_grad()"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 159,
   "metadata": {},
   "outputs": [],
   "source": [
    "fake_noise = get_noise(len(real), z_dim, device=device)\n",
    "noise_and_labels = combine_vectors(fake_noise, one_hot_labels)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 160,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/plain": [
       "torch.Size([8, 70])"
      ]
     },
     "execution_count": 160,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "noise_and_labels.shape"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 161,
   "metadata": {},
   "outputs": [],
   "source": [
    "fake = gen(noise_and_labels)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 162,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/plain": [
       "torch.Size([8, 3, 96, 96])"
      ]
     },
     "execution_count": 162,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "fake.shape"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 163,
   "metadata": {},
   "outputs": [],
   "source": [
    "fake_image_and_labels = combine_vectors(fake, image_one_hot_labels).detach()\n",
    "real_image_and_labels = combine_vectors(real, image_one_hot_labels)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 164,
   "metadata": {},
   "outputs": [],
   "source": [
    "disc_fake_pred = disc(fake_image_and_labels)\n",
    "disc_real_pred = disc(real_image_and_labels)\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 176,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/plain": [
       "tensor([[-1.6933],\n",
       "        [ 1.1524],\n",
       "        [-1.3028],\n",
       "        [ 0.0396],\n",
       "        [ 1.8728],\n",
       "        [ 1.2734],\n",
       "        [-0.2312],\n",
       "        [-0.1191]], device='cuda:0', grad_fn=<ViewBackward>)"
      ]
     },
     "execution_count": 176,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "disc_fake_pred"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 166,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/plain": [
       "tensor([[-0.1401],\n",
       "        [-1.1365],\n",
       "        [-1.8309],\n",
       "        [ 0.0347],\n",
       "        [-1.9482],\n",
       "        [-0.5223],\n",
       "        [ 1.8726],\n",
       "        [ 0.2322]], device='cuda:0', grad_fn=<ViewBackward>)"
      ]
     },
     "execution_count": 166,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "disc_real_pred"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 167,
   "metadata": {},
   "outputs": [],
   "source": [
    "epsilon =  torch.rand(len(real), 1, 1, 1, device=device, requires_grad=True)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 169,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/plain": [
       "torch.Size([8, 1, 1, 1])"
      ]
     },
     "execution_count": 169,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "epsilon.shape"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 170,
   "metadata": {},
   "outputs": [],
   "source": [
    "grad = get_gradient(disc, real, fake.detach(), epsilon)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 172,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/plain": [
       "torch.Size([8, 3, 96, 96])"
      ]
     },
     "execution_count": 172,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "grad.shape"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 173,
   "metadata": {},
   "outputs": [],
   "source": [
    "gp = gradient_penalty(grad)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 174,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/plain": [
       "tensor(87.7597, device='cuda:0', grad_fn=<PowBackward0>)"
      ]
     },
     "execution_count": 174,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "gp"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 180,
   "metadata": {},
   "outputs": [],
   "source": [
    "disc_loss = -disc_real_pred.mean() + disc_fake_pred.mean() + gp * gp_weight"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 182,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/plain": [
       "878.1505126953125"
      ]
     },
     "execution_count": 182,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "disc_loss.item()"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "\n",
    "\n",
    "# gradient penalty\n",
    "epsilon = torch.rand(len(real), 1, 1, 1, device=device, requires_grad=True)\n",
    "grad = get_gradient(disc, real, fake.detach(), epsilon)\n",
    "gp = gradient_penalty(grad)\n",
    "disc_loss = -disc_real_pred.mean() + disc_fake_pred.mean() + gp * gp_weight"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 191,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/plain": [
       "3.0"
      ]
     },
     "execution_count": 191,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "real_mean = (1+2+3+4+5)/5\n",
    "real_mean"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 192,
   "metadata": {},
   "outputs": [],
   "source": [
    "mean_iteration_disc_loss = 0"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 205,
   "metadata": {},
   "outputs": [],
   "source": [
    "disc_loss = 5"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 206,
   "metadata": {},
   "outputs": [],
   "source": [
    "mean_iteration_disc_loss += disc_loss / 5"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 207,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/plain": [
       "3.0"
      ]
     },
     "execution_count": 207,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "mean_iteration_disc_loss"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "\n",
    "\n",
    "# TRAINING\n",
    "\n",
    "generator_losses = []\n",
    "discriminator_losses = []\n",
    "for epoch in range(n_epochs):\n",
    "    # i is the batch number\n",
    "    for i, (real, labels) in enumerate(tqdm(dataloader)):\n",
    "        real = real.to(device)\n",
    "        \n",
    "        # get the one-hot labels for the gen and disc\n",
    "        one_hot_labels = get_one_hot_labels(labels.to(device), n_classes)\n",
    "        image_one_hot_labels = one_hot_labels[:, :, None, None]\n",
    "        image_one_hot_labels = image_one_hot_labels.repeat(1, 1, data_shape[1], data_shape[2])\n",
    "        \n",
    "        mean_iteration_disc_loss = 0\n",
    "        for _ in range(disc_repeats):\n",
    "            \n",
    "            # ====================\n",
    "            # UPDATE DISCRIMINATOR\n",
    "            # ====================\n",
    "            \n",
    "            # zero out the gradients\n",
    "            opt_D.zero_grad()\n",
    "       \n",
    "            # get the noise\n",
    "            fake_noise = get_noise(len(real), z_dim, device=device)\n",
    "        \n",
    "            # concatenate the noise to the one-hot labels\n",
    "            noise_and_labels = combine_vectors(fake_noise, one_hot_labels)\n",
    "        \n",
    "            # generate the fakes\n",
    "            fake = gen(noise_and_labels)\n",
    "        \n",
    "            # concatenate the images to the labels (make sure to detach the fakes)\n",
    "            fake_image_and_labels = combine_vectors(fake, image_one_hot_labels).detach()\n",
    "            real_image_and_labels = combine_vectors(real, image_one_hot_labels)\n",
    "        \n",
    "            # get the discriminator predictions\n",
    "            disc_fake_pred = disc(fake_image_and_labels)\n",
    "            disc_real_pred = disc(real_image_and_labels)\n",
    "            \n",
    "            # gradient penalty\n",
    "            epsilon = torch.rand(len(real), 1, 1, 1, device=device, requires_grad=True)\n",
    "            grad = get_gradient(disc, real, fake.detach(), epsilon)\n",
    "            gp = gradient_penalty(grad)\n",
    "            disc_loss = -disc_real_pred.mean() + disc_fake_pred.mean() + gp * gp_weight\n",
    "            \n",
    "            # keep track of the average disc loss in this batch\n",
    "            mean_iteration_disc_loss += disc_loss.item() / disc_repeats\n",
    "            \n",
    "            # update gradients\n",
    "            disc_loss.backward(retain_graph=True)\n",
    "            \n",
    "            # update optimizer\n",
    "            opt_D.step()\n",
    "        discriminator_losses += [mean_iteration_disc_loss]\n",
    "            \n",
    "      \n",
    "        # ================\n",
    "        # UPDATE GENERATOR \n",
    "        # ================\n",
    "        \n",
    "        # zero out the gradients\n",
    "        opt_G.zero_grad()\n",
    "        \n",
    "        # get new noise\n",
    "#         fake_noise_2 = get_noise(len(real), z_dim, device=device)\n",
    "#         noise_and_labels_2 = combine_vectors(fake_noise_2, one_hot_labels)\n",
    "#         fake_2 = gen(noise_and_labels_2)\n",
    "\n",
    "        # concatenate the fakes to the one-hot img labels\n",
    "        fake_image_and_labels_2 = combine_vectors(fake, image_one_hot_labels)\n",
    "        \n",
    "        # get the predictions for the fakes\n",
    "        disc_fake_pred = disc(fake_image_and_labels_2)\n",
    "        \n",
    "        # get gen loss\n",
    "        gen_loss = -disc_fake_pred.mean()\n",
    "        \n",
    "        # backpropagation to compute gradients for all layers\n",
    "        gen_loss.backward()\n",
    "        \n",
    "        # update weights for this batch\n",
    "        opt_G.step()\n",
    "        \n",
    "        # current epoch nr * iter per epochs + iterations in current loop \n",
    "        batches_per_epoch = len(dataloader)\n",
    "        total_finished_batches = (epoch + 1) * (batches_per_epoch) + (i + 1)\n",
    "        \n",
    "        # save loss in list after each epoch\n",
    "        if total_finished_batches % batches_per_epoch == 0:\n",
    "            discriminator_losses += [mean_iteration_disc_loss] \n",
    "            generator_losses += [gen_loss.item()]\n",
    "        \n",
    "        # only save gen/loss progress images every save_nr epochs (if 5000 epochs)\n",
    "        if total_finished_batches % (batches_per_epoch * save_nr) == 0:\n",
    "            save_mixed_images(25, epoch, gen_imgs_path)\n",
    "            plot_losses(generator_losses, discriminator_losses, loss_plots_path)\n",
    "        if (epoch + 1) > checkpoint_start and total_finished_batches % (batches_per_epoch * checkpoint_nr) == 0:\n",
    "            torch.save({\n",
    "                'G_state_dict': gen.state_dict(),\n",
    "                'D_state_dict': disc.state_dict(),\n",
    "                'opt_G': opt_G.state_dict(),\n",
    "                'opt_D': opt_D.state_dict()\n",
    "            }, checkpoints_path + '/chkpt_epoch%d.pt' % (epoch + 1))\n",
    "\n",
    "    # print the numbers after each epoch\n",
    "    print('[Epoch %d/%d] [D loss: %f] [G loss: %f]' % \n",
    "    (epoch+1,  n_epochs,\n",
    "    discriminator_losses[-1], generator_losses[-1]))\n"
   ]
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Python 3",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.6.9"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 4
}
