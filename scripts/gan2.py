
# CHANGES TOWARDS BASELINE
# 96x96 imgs instead of 200x200 (added PIL in preprocessing)
# generator architecture from small to large nr of neurons
# added a new argument (imgsize)
# changed show function

import argparse
import os
from os import listdir
from os.path import isfile, join
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
import random
import math
import warnings
import math
warnings.filterwarnings('ignore')
import cv2
from PIL import Image

import torch
from torch.utils.data import Dataset, DataLoader
from torch import nn
from tqdm import tqdm
from torchvision import transforms
from torchvision.utils import make_grid
from torchvision.utils import save_image
import torch.nn.functional as F
torch.manual_seed(0)

parser = argparse.ArgumentParser()
parser.add_argument('--n_epochs', type=int, default=200, help='number of epochs of training')
parser.add_argument('--batch_size', type=int, default=256, help='size of the batches')
parser.add_argument("--lr", type=float, default=0.0002, help="adam: learning rate")
parser.add_argument("--b1", type=float, default=0.5, help="adam: decay of first order momentum of gradient")
parser.add_argument("--b2", type=float, default=0.999, help="adam: decay of first order momentum of gradient")
parser.add_argument("--z_dim", type=int, default=64, help="dimensionality of the latent space")
parser.add_argument("--csv_file", type=str, default='id_and_label_10000.csv', help="name of the csv file")
parser.add_argument("--model_name", type=str, default='base1', help="name of the model")
parser.add_argument("--img_size", type=int, default=96, help="size of the model")
parser.add_argument("--save_nr", type=int, default=50, help="save the images after this number of epochs")
parser.add_argument("--n_classes", type=int, default=2, help="number of classes in the dataset")
parser.add_argument("--checkpoint_nr", type=int, default=10, help="save model after checkpoint_start + this number of epochs")
parser.add_argument("--checkpoint_start", type=int, default=200, help="start saving the model after this number of epochs")
args = parser.parse_args()
print(args)

# HYPERPARAMETERS
n_epochs = args.n_epochs
z_dim = args.z_dim
batch_size = args.batch_size
lr = args.lr
b1 = args.b1
b2 = args.b1
model_name = args.model_name
csv_file = args.csv_file
img_size = args.img_size
save_nr = args.save_nr
n_classes = args.n_classes
checkpoint_nr = args.checkpoint_nr
checkpoint_start = args.checkpoint_start

# FIXED PARAMETERS
criterion = nn.BCEWithLogitsLoss()
device = 'cuda'
data_shape = (3, img_size, img_size)


cuda = True if torch.cuda.is_available() else False

# DEFINE THE PATH FOR RESULTS
result_path = '/data1/Itzel/GAN_results/' + model_name
# result_path = 'D:/GAN_results/model_' + model_name

loss_plots_path = result_path + '/loss_plots'
gen_imgs_path = result_path + '/gen_imgs'
checkpoints_path = result_path + '/checkpoints'
if not os.path.exists(result_path):
    os.makedirs(result_path)
if not os.path.exists(loss_plots_path):
    os.makedirs(loss_plots_path)
if not os.path.exists(gen_imgs_path):
    os.makedirs(gen_imgs_path)
if not os.path.exists(checkpoints_path):
    os.makedirs(checkpoints_path)



# DATASET
class HeerlenDataset(Dataset):
    def __init__(self, csv_file, root_dir, transform=None):
        self.annotations = pd.read_csv(csv_file)
        self.root_dir = root_dir
        self.transform = transform
    
    def __len__(self):
        return len(self.annotations)
    
    def __getitem__(self, index):
        img_path = os.path.join(self.root_dir, self.annotations.iloc[index, 0])
        image = cv2.imread(img_path)
        image =  cv2.cvtColor(image, cv2.COLOR_BGR2RGB)
        image = Image.fromarray(image)
        y_label = torch.tensor(int(self.annotations.iloc[index, 1]))
        
        if self.transform:
            image = self.transform(image)
        
        return (image, y_label)

    

# GET THE DATASET
transform = transforms.Compose([
    transforms.Resize(img_size),
    transforms.ToTensor(),
    # mean = 0.5, std = 0.5, from (0,1) to (-1,1)
    transforms.Normalize((0.5,0.5,0.5), (0.5,0.5,0.5)) 
])

#dataset_path = 'D:/Heerlen_HR_2018/Heerlen_HR_2018/Heerlen_HR_2018/full/'
dataset_path = '/data1/Heerlen_HR_2018/full/'
dataset = HeerlenDataset(csv_file= '/data1/Itzel/' + csv_file,
                                      root_dir=dataset_path,
                                      transform=transform)


# GENERATOR
class Generator(nn.Module):

    def __init__(self, im_chan=3, hidden_dim=64):
        super(Generator, self).__init__()
        self.input_dim =  z_dim + n_classes
        # Build the neural network
        self.gen = nn.Sequential(
            self.block(self.input_dim, 32, kernel_size=4, stride=2),
            self.block(32, 64, kernel_size=4, stride=2),
            self.block(64, 128, kernel_size=4, stride=2),
            self.block(128, 256, kernel_size=5, stride=2),
            self.block(256, 3, kernel_size=4, stride=2, final_layer=True),
        )

    def block(self, input_channels, output_channels,kernel_size=3, stride=2, final_layer=False):

        if not final_layer:
            return nn.Sequential(
                nn.ConvTranspose2d(input_channels, output_channels, kernel_size, stride),
                nn.BatchNorm2d(output_channels),
                nn.ReLU(inplace=True),
            )
        else:
            return nn.Sequential(
                nn.ConvTranspose2d(input_channels, output_channels, kernel_size, stride),
                nn.Tanh(),
            )

    def forward(self, noise):

        gen_input = noise.view(len(noise),  z_dim + n_classes, 1, 1) # creates a (n_samples, 64 + 2, 1, 1) tensor
        return self.gen(gen_input)

def get_noise(n_samples, input_dim, device='cpu'):

    return torch.randn(n_samples, input_dim, device=device)


# DISCRIMINATOR
class Discriminator(nn.Module):

    def __init__(self):
        super(Discriminator, self).__init__()
        self.input_dim = data_shape[0] + n_classes 
        self.disc = nn.Sequential(
            self.block(self.input_dim, 64),
            self.block(64, 128),
            self.block(128, 256),
            self.block(256, 512),
            self.block(512, 3, final_layer=True),
        )

    def block(self, input_channels, output_channels, kernel_size=4, stride=2, final_layer=False):

        if not final_layer:
            return nn.Sequential(
                nn.Conv2d(input_channels, output_channels, kernel_size, stride),
                nn.BatchNorm2d(output_channels),
                nn.LeakyReLU(0.2, inplace=True),
            )
        else:
            return nn.Sequential(
                nn.Conv2d(input_channels, output_channels, kernel_size, stride),
            )

    def forward(self, image):

        disc_pred = self.disc(image)
        return disc_pred.view(len(disc_pred), -1)

    

# SOME HELPER FUNCTIONS
def get_one_hot_labels(labels, n_classes):
    return F.one_hot(labels,n_classes)

def combine_vectors(x, y):
    combined = torch.cat([x.float(),y.float()], dim=1)
    return combined



# EVALUATION FUNCTIONS
def show_image(img):
    
    # transform back from (-1,1) to (0,1)
    img = (img + 1) / 2 
    img = img.detach().cpu()
    
    # change from (3,size,size) to (size, size, 3)
    plt.imshow(img.permute(1, 2, 0).squeeze())
    plt.show()

# ONLY IN CASE OF 2 CLASSES (NON-CLUSTERS)
# def save_sample_images(nr_images, epoch, gen_imgs_path):

#     # create the one-hot positive labels
#     label_shape = torch.empty(nr_images) # check if this is the correct label shape
#     labels = torch.ones_like(input=label_shape, dtype=torch.int64) # we only want to have positives   
#     one_hot_labels = F.one_hot(labels.to(device),n_classes)

#     # noise
#     noise = get_noise(nr_images, z_dim, device=device)
#     noise_and_labels = torch.cat([noise.float(),one_hot_labels.float()],
#                                  dim=1)

#     # get the generated images and transform back to original 
#     gen_imgs = gen(noise_and_labels)
#     nrow = int(np.sqrt(nr_images))
#     epochs_finished = epoch + 2
#     save_image(tensor=gen_imgs.data,
#                fp=gen_imgs_path + '/images_epoch%d.png' % (epochs_finished),
#               normalize=True,
#                nrow=nrow)

def save_mixed_images(nr_images, epoch, gen_imgs_path):
    
    for i in range(5):
        
        # create the one-hot labels per cluster
        exec(f'label_shape{i} = torch.empty(round(nr_images/5))')
        exec(f'labels{i} = label_shape{i}.fill_({i}).to(torch.int64)')
        exec(f'one_hot_labels{i} = F.one_hot(labels{i}.to(device),n_classes)')
        
        # get the noise per cluster
        exec(f'noise{i} = get_noise(round(nr_images/5), z_dim, device=device)')
        exec(f'noise_and_labels{i} = torch.cat([noise{i}.float(),one_hot_labels{i}.float()],dim=1)')
        
        # generate images per noise
        exec(f'gen_imgs{i} = gen(noise_and_labels{i})')
    
    # concatenate the generated images and transform back to original
    all_imgs = torch.cat([gen_imgs0, gen_imgs1, gen_imgs2, gen_imgs3, gen_imgs4], dim=0)
    nrow = int(np.sqrt(nr_images))
    epochs_finished = epoch + 2
    save_image(tensor=gen_imgs.data,
               fp=gen_imgs_path + '/images_epoch%d.png' % (epochs_finished),
              normalize=True,
               nrow=nrow) 

def plot_losses(generator_losses, discriminator_losses, loss_plots_path):
    fig = plt.figure()
    epochs_finished = epoch + 2
    plt.plot(generator_losses[-100:], label='Generator loss')
    plt.plot(discriminator_losses[-100:], label='Discriminator loss')
    plt.title('Losses')
    plt.xlabel('Last 100 epochs')
    plt.ylabel('Loss')
    plt.legend()
    plt.savefig(loss_plots_path + '/losses_epoch%d.png' % epochs_finished)
    plt.close(fig)


# INITIALIZE GEN AND DISC

gen = Generator().to(device)
disc = Discriminator().to(device)


# OPTIMIZERS
opt_G = torch.optim.Adam(gen.parameters(), lr= lr, betas=(b1, b2))
opt_D = torch.optim.Adam(disc.parameters(), lr= lr, betas=(b1, b2))


# INITIALIZE THE WEIGHTS

def weights_init(x):
    # if conv2d or convtranspose2d
    if isinstance(x, nn.Conv2d) or isinstance(x, nn.ConvTranspose2d):
        torch.nn.init.normal_(x.weight, 0.0, 0.02)
    # if batchnorm
    if isinstance(x, nn.BatchNorm2d):
        torch.nn.init.normal_(x.weight, 1.0, 0.02)
        torch.nn.init.constant_(x.bias, 0)

gen = gen.apply(weights_init)
disc = disc.apply(weights_init)



# DATASET LOADER
dataloader = DataLoader(dataset=dataset,
                         batch_size=batch_size,
                         shuffle=True)

# TRAINING

generator_losses = []
discriminator_losses = []
for epoch in range(n_epochs):
    # i is the batch number
    for i, (real, labels) in enumerate(tqdm(dataloader)):
        
        real = real.to(device)

        # get the one-hot labels for the gen and disc
        one_hot_labels = get_one_hot_labels(labels.to(device), n_classes)
        image_one_hot_labels = one_hot_labels[:, :, None, None]
        image_one_hot_labels = image_one_hot_labels.repeat(1, 1, data_shape[1], data_shape[2])

        # ====================
        # UPDATE DISCRIMINATOR
        # ====================
        
        # zero out the gradients
        opt_D.zero_grad()
        
        # get the noise
        fake_noise = get_noise(len(real), z_dim, device=device)
        
        # concatenate the noise to the one-hot labels
        noise_and_labels = combine_vectors(fake_noise, one_hot_labels)
        
        # generate the fakes
        fake = gen(noise_and_labels)
        
        # concatenate the images to the labels (make sure to detach the fakes)
        fake_image_and_labels = combine_vectors(fake, image_one_hot_labels).detach()
        real_image_and_labels = combine_vectors(real, image_one_hot_labels)
        
        # get the discriminator predictions
        disc_fake_pred = disc(fake_image_and_labels)
        disc_real_pred = disc(real_image_and_labels)
        
        # compute the disc losses
        disc_fake_loss = criterion(disc_fake_pred, torch.zeros_like(disc_fake_pred))
        disc_real_loss = criterion(disc_real_pred, torch.ones_like(disc_real_pred))
        
        # compute the avg loss
        disc_loss = (disc_fake_loss + disc_real_loss) / 2
        
        # backpropagation to compute gradients for all layers
        disc_loss.backward(retain_graph=True)
        
        # update weights for this batch
        opt_D.step() 
        
        # ================
        # UPDATE GENERATOR 
        # ================
        
        # zero out the gradients
        opt_G.zero_grad()
        
        # concatenate the fakes to the one-hot img labels
        fake_image_and_labels = combine_vectors(fake, image_one_hot_labels)
        
        # get the predictions for the fakes
        disc_fake_pred = disc(fake_image_and_labels)
        
        # gen wants the disc to think its fakes are real (pred 1)
        gen_loss = criterion(disc_fake_pred, torch.ones_like(disc_fake_pred))
        
        # backpropagation to compute gradients for all layers
        gen_loss.backward()
        
        # update weights for this batch
        opt_G.step()

        # current epoch nr * iter per epochs + iterations in current loop 
        batches_per_epoch = len(dataloader)
        total_finished_batches = (epoch + 1) * (batches_per_epoch) + (i + 1)
        
        # save loss in list after each epoch
        if total_finished_batches % batches_per_epoch == 0:
            discriminator_losses += [disc_loss.item()]
            generator_losses += [gen_loss.item()]
        
        # only save gen/loss progress images every save_nr epochs (if 5000 epochs)
        if total_finished_batches % (batches_per_epoch * save_nr) == 0:
            save_mixed_images(25, epoch, gen_imgs_path)
            plot_losses(generator_losses, discriminator_losses, loss_plots_path)
        if (epoch + 1) > checkpoint_start and total_finished_batches % (batches_per_epoch * checkpoint_nr) == 0:
            torch.save({
                'G_state_dict': gen.state_dict(),
                'D_state_dict': disc.state_dict(),
                'opt_G': opt_G.state_dict(),
                'opt_D': opt_D.state_dict()
            }, checkpoints_path + '/chkpt_epoch%d.pt' % (epoch + 2))

    # print the numbers after each epoch
    print('[Epoch %d/%d] [D loss: %f] [G loss: %f]' % 
    (epoch+1,  n_epochs,
    discriminator_losses[-1], generator_losses[-1]))




