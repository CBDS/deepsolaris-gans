import argparse
import os
from os import listdir
from os.path import isfile, join
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
import random
import math
import warnings
import math
warnings.filterwarnings('ignore')
import cv2
from PIL import Image
import torch
from torch.utils.data import Dataset, DataLoader
from torch import nn
from tqdm import tqdm
from torchvision import transforms
from torchvision.utils import make_grid
from torchvision.utils import save_image
import torch.nn.functional as F
torch.manual_seed(0)

# torch.set_num_threads(10)

parser = argparse.ArgumentParser()
parser.add_argument('--n_epochs', type=int, default=200, help='number of epochs of training')
parser.add_argument('--batch_size', type=int, default=5, help='size of the batches (default: nr of classes)')
parser.add_argument("--lr", type=float, default=0.0002, help="adam: learning rate")
parser.add_argument("--b1", type=float, default=0.5, help="adam: decay of first order momentum of gradient")
parser.add_argument("--b2", type=float, default=0.999, help="adam: decay of first order momentum of gradient")
parser.add_argument("--z_dim", type=int, default=120, help="dimensionality of the latent space")
parser.add_argument("--csv_file", type=str, default='10000_kmeans_pos_omitted.csv', help="name of the csv file")
parser.add_argument("--model_name", type=str, default='model_biggan_1', help="name of the model")
parser.add_argument("--img_size", type=int, default=128, help="size of the image")
parser.add_argument("--save_nr", type=int, default=10, help="save the images after this number of epochs")
parser.add_argument("--n_classes", type=int, default=5, help="number of classes in the dataset")
parser.add_argument("--checkpoint_nr", type=int, default=10, help="save model after checkpoint_start + this number of epochs")
parser.add_argument("--checkpoint_start", type=int, default=100, help="start saving the model after this number of epochs")
parser.add_argument("--disc_repeats", type=int, default=5, help="number of times to update disc before updating gen")
parser.add_argument("--gp_weight", type=int, default=10, help="weight of gradient penalty")
parser.add_argument("--base_channels", type=int, default=96, help="base channels")
parser.add_argument("--shared_dim", type=int, default=128, help="shared dimension")
parser.add_argument("--company_device", type=bool, default=True, help="whether to run on company GPU")
args = parser.parse_args()
print(args)

n_epochs = args.n_epochs
z_dim = args.z_dim
batch_size = args.batch_size
lr = args.lr
b1 = args.b1
b2 = args.b1
model_name = args.model_name
csv_file = args.csv_file
img_size = args.img_size
save_nr = args.save_nr
n_classes = args.n_classes
checkpoint_nr = args.checkpoint_nr
checkpoint_start = args.checkpoint_start
company_device = args.company_device
save_nr = args.save_nr
base_channels = args.base_channels
shared_dim = args.shared_dim

# FIXED PARAMETERS
criterion = nn.BCEWithLogitsLoss()
device = 'cuda'
data_shape = (3, img_size, img_size)

cuda = True if torch.cuda.is_available() else False



# CREATE DIRECTORIES
if company_device:
    result_path = '/data1/Itzel/GAN_results/' + model_name
else:
    result_path = 'D:/GAN_results/model_' + model_name
    
loss_plots_path = result_path + '/loss_plots'
gen_imgs_path = result_path + '/gen_imgs'
checkpoints_path = result_path + '/checkpoints'
if not os.path.exists(result_path):
    os.makedirs(result_path)
if not os.path.exists(loss_plots_path):
    os.makedirs(loss_plots_path)
if not os.path.exists(gen_imgs_path):
    os.makedirs(gen_imgs_path)
if not os.path.exists(checkpoints_path):
    os.makedirs(checkpoints_path)

    
    
# DATASET
class HeerlenDataset(Dataset):
    def __init__(self, csv_file, root_dir, transform=None):
        self.annotations = pd.read_csv(csv_file)
        self.root_dir = root_dir
        self.transform = transform
    
    def __len__(self):
        return len(self.annotations)
    
    def __getitem__(self, index):
        img_path = os.path.join(self.root_dir, self.annotations.iloc[index, 0])
        image = cv2.imread(img_path)
        image =  cv2.cvtColor(image, cv2.COLOR_BGR2RGB)
        image = Image.fromarray(image)
        y_label = torch.tensor(int(self.annotations.iloc[index, 1]))
        
        if self.transform:
            image = self.transform(image)
        
        return (image, y_label)

# GET THE DATASET
transform = transforms.Compose([
    transforms.Resize(img_size),
    transforms.ToTensor(),
    # mean = 0.5, std = 0.5, from (0,1) to (-1,1)
    transforms.Normalize((0.5,0.5,0.5), (0.5,0.5,0.5)) 
])

if company_device:
    dataset_path = '/data1/Heerlen_HR_2018/full/'
    dataset = HeerlenDataset(csv_file= '/data1/Itzel/csv_files/' + csv_file,
                             root_dir=dataset_path,
                             transform=transform)
else:
    dataset_path = 'D:/Heerlen_HR_2018/Heerlen_HR_2018/Heerlen_HR_2018/full/'
    dataset = HeerlenDataset(csv_file= '../' + csv_file,
                                      root_dir=dataset_path,
                                      transform=transform)
    
    
    
    
# EVALUATION FUNCTIONS
def show_image(img):
    
    # transform back from (-1,1) to (0,1)
    img = (img + 1) / 2 
    img = img.detach().cpu()
    
    # change from (3,size,size) to (size, size, 3)
    plt.imshow(img.permute(1, 2, 0).squeeze())
    plt.show()
    
    
def save_mixed_images(nr_images, epoch, gen_imgs_path):
    
    imgs_list = []
    
    for i in range(5):

        # create the one-hot labels per cluster
        label_shape = torch.empty(round(nr_images/5))
        labels = label_shape.fill_(i).to(torch.int64)
        labels = labels.to(device)
        
        # get the noise per cluster
        z = get_noise(round(nr_images/5), z_dim, device=device)
        
        # get class embeddings (y_emb) from generator
        y_emb = gen.shared_emb(labels)
        
        # generate images per noise
        gen_imgs = gen(z, y_emb)
        imgs_list.append(gen_imgs)

    
    # concatenate the generated images and transform back to original
    all_imgs = torch.cat([imgs_list[0], imgs_list[1], imgs_list[2], imgs_list[3], imgs_list[4]], dim=0)
    nrow = int(np.sqrt(nr_images))
    epochs_finished = epoch + 2
    save_image(tensor=all_imgs.data,
               fp=gen_imgs_path + '/images_epoch%d.png' % (epochs_finished),
              normalize=True,
               nrow=nrow) 


def save_sample_images(nr_images, epoch, gen_imgs_path):
    
    # create the one-hot positive labels
    label_shape = torch.empty(nr_images) # check if this is the correct label shape
    labels = torch.ones_like(input=label_shape, dtype=torch.int64) # we only want to have positives   
    one_hot_labels = F.one_hot(labels.to(device),n_classes)

    # noise
    noise = get_noise(nr_images, z_dim, device=device)
    noise_and_labels = torch.cat([noise.float(),one_hot_labels.float()],
                                 dim=1)

    # get the generated images and transform back to original 
    gen_imgs = gen(noise_and_labels)
    nrow = int(np.sqrt(nr_images))
    epochs_finished = epoch + 1
    save_image(tensor=gen_imgs.data,
               fp=gen_imgs_path + '/images_epoch%d.png' % (epochs_finished),
              normalize=True,
               nrow=nrow)
    


def plot_losses(generator_losses, discriminator_losses, loss_plots_path):
    fig = plt.figure()
    epochs_finished = epoch + 1
    plt.plot(generator_losses[-50:], label='Generator loss')
    plt.plot(discriminator_losses[-50:], label='Discriminator loss')
    plt.title('Losses')
    plt.xlabel('Last 50 epochs')
    plt.ylabel('Loss')
    plt.legend()
    plt.savefig(loss_plots_path + '/losses_epoch%d.png' % epochs_finished)
    plt.close(fig)
    
def get_noise(n_samples, input_dim, device='cpu'):
    return torch.randn(n_samples, input_dim, device=device)

    
    
# MODEL CREATION

class ClassConditionalBatchNorm2d(nn.Module):
    '''
    in_channels: the dimension of the class embedding (c) + noise vector (z)
    out_channels: the dimension of the activation tensor to be normalized
    '''

    def __init__(self, in_channels, out_channels):
        super().__init__()
        self.bn = torch.nn.BatchNorm2d(out_channels)
        self.class_scale_transform = nn.utils.spectral_norm(nn.Linear(in_channels, out_channels, bias=False))
        self.class_shift_transform = nn.utils.spectral_norm(nn.Linear(in_channels, out_channels, bias=False))

    def forward(self, x, y):
        normalized_image = self.bn(x)
        class_scale = (1 + self.class_scale_transform(y))[:, :, None, None]
        class_shift = self.class_shift_transform(y)[:, :, None, None]
        transformed_image = class_scale * normalized_image + class_shift
        return transformed_image


class AttentionBlock(nn.Module):

    def __init__(self, channels):
        super().__init__()

        self.channels = channels

        self.theta = nn.utils.spectral_norm(nn.Conv2d(channels, channels // 8, kernel_size=1, padding=0, bias=False))
        self.phi = nn.utils.spectral_norm(nn.Conv2d(channels, channels // 8, kernel_size=1, padding=0, bias=False))
        self.g = nn.utils.spectral_norm(nn.Conv2d(channels, channels // 2, kernel_size=1, padding=0, bias=False))
        self.o = nn.utils.spectral_norm(nn.Conv2d(channels // 2, channels, kernel_size=1, padding=0, bias=False))

        self.gamma = nn.Parameter(torch.tensor(0.), requires_grad=True)

    def forward(self, x):
        spatial_size = x.shape[2] * x.shape[3]

        # Apply convolutions to get query (theta), key (phi), and value (g) transforms
        theta = self.theta(x)
        phi = F.max_pool2d(self.phi(x), kernel_size=2)
        g = F.max_pool2d(self.g(x), kernel_size=2)

        # Reshape spatial size for self-attention
        theta = theta.view(-1, self.channels // 8, spatial_size)
        phi = phi.view(-1, self.channels // 8, spatial_size // 4)
        g = g.view(-1, self.channels // 2, spatial_size // 4)

        # Compute dot product attention with query (theta) and key (phi) matrices
        beta = F.softmax(torch.bmm(theta.transpose(1, 2), phi), dim=-1)

        # Compute scaled dot product attention with value (g) and attention (beta) matrices
        o = self.o(torch.bmm(g, beta.transpose(1, 2)).view(-1, self.channels // 2, x.shape[2], x.shape[3]))

        # Apply gain and residual
        return self.gamma * o + x
    
class GResidualBlock(nn.Module):
    '''
    c_dim: the dimension of conditional vector [c, z]
    in_channels: the number of channels in the input
    out_channels: the number of channels in the output
    '''

    def __init__(self, c_dim, in_channels, out_channels):
        super().__init__()

        self.conv1 = nn.utils.spectral_norm(nn.Conv2d(in_channels, out_channels, kernel_size=3, padding=1))
        self.conv2 = nn.utils.spectral_norm(nn.Conv2d(out_channels, out_channels, kernel_size=3, padding=1))

        self.bn1 = ClassConditionalBatchNorm2d(c_dim, in_channels)
        self.bn2 = ClassConditionalBatchNorm2d(c_dim, out_channels)

        self.activation = nn.ReLU()
        self.upsample_fn = nn.Upsample(scale_factor=2)     # upsample occurs in every gblock

        self.mixin = (in_channels != out_channels)
        if self.mixin:
            self.conv_mixin = nn.utils.spectral_norm(nn.Conv2d(in_channels, out_channels, kernel_size=1, padding=0))

    def forward(self, x, y):
        # h := upsample(x, y)
        h = self.bn1(x, y)
        h = self.activation(h)
        h = self.upsample_fn(h)
        h = self.conv1(h)

        # h := conv(h, y)
        h = self.bn2(h, y)
        h = self.activation(h)
        h = self.conv2(h)

        # x := upsample(x)
        x = self.upsample_fn(x)
        if self.mixin:
            x = self.conv_mixin(x)

        return h + x
    
class Generator(nn.Module):
    '''
    z_dim: the dimension of random noise sampled
    shared_dim: the dimension of shared class embeddings
    base_channels: the number of base channels
    bottom_width: the height/width of image before it gets upsampled
    n_classes: the number of image classes
    '''
 
    def __init__(self, base_channels=96, bottom_width=4, z_dim=120, shared_dim=128, n_classes=5):
        super().__init__()
 
        n_chunks = 6    # 5 (generator blocks) + 1 (generator input)
        self.z_chunk_size = z_dim // n_chunks
        self.z_dim = z_dim
        self.shared_dim = shared_dim
        self.bottom_width = bottom_width
 
        # No spectral normalization on embeddings, which authors observe to cripple the generator
        self.shared_emb = nn.Embedding(n_classes, shared_dim)
 
        self.proj_z = nn.Linear(self.z_chunk_size, 16 * base_channels * bottom_width ** 2)
 
        # Can't use one big nn.Sequential since we are adding class+noise at each block
        self.g_blocks = nn.ModuleList([
            nn.ModuleList([
                GResidualBlock(shared_dim + self.z_chunk_size, 16 * base_channels, 16 * base_channels),
                AttentionBlock(16 * base_channels),
            ]),
            nn.ModuleList([
                GResidualBlock(shared_dim + self.z_chunk_size, 16 * base_channels, 8 * base_channels),
                AttentionBlock(8 * base_channels),
            ]),
            nn.ModuleList([
                GResidualBlock(shared_dim + self.z_chunk_size, 8 * base_channels, 4 * base_channels),
                AttentionBlock(4 * base_channels),
            ]),
            nn.ModuleList([
                GResidualBlock(shared_dim + self.z_chunk_size, 4 * base_channels, 2 * base_channels),
                AttentionBlock(2 * base_channels),
            ]),
            nn.ModuleList([
                GResidualBlock(shared_dim + self.z_chunk_size, 2 * base_channels, base_channels),
                AttentionBlock(base_channels),
            ]),
        ])
        self.proj_o = nn.Sequential(
            nn.BatchNorm2d(base_channels),
            nn.ReLU(inplace=True),
            nn.utils.spectral_norm(nn.Conv2d(base_channels, 3, kernel_size=1, padding=0)),
            nn.Tanh(),
        )
 
    def forward(self, z, y):
        '''
        z: random noise with size self.z_dim
        y: class embeddings with size self.shared_dim
            = NOTE =
            y should be class embeddings from self.shared_emb, not the raw class labels
        '''
        # Chunk z and concatenate to shared class embeddings
        zs = torch.split(z, self.z_chunk_size, dim=1)
        z = zs[0]
        ys = [torch.cat([y, z], dim=1) for z in zs[1:]]
 
        # Project noise and reshape to feed through generator blocks
        h = self.proj_z(z)
        h = h.view(h.size(0), -1, self.bottom_width, self.bottom_width)
 
        # Feed through generator blocks
        for idx, g_block in enumerate(self.g_blocks):
            h = g_block[0](h, ys[idx])
            h = g_block[1](h)
 
        # Project to 3 RGB channels with tanh to map values to [-1, 1]
        h = self.proj_o(h)
 
        return h

class DResidualBlock(nn.Module):
    '''
    in_channels: the number of channels in the input
    out_channels: the number of channels in the output
    downsample: whether to apply downsampling
    use_preactivation: whether to apply an activation function before the first convolution
    '''

    def __init__(self, in_channels, out_channels, downsample=True, use_preactivation=False):
        super().__init__()

        self.conv1 = nn.utils.spectral_norm(nn.Conv2d(in_channels, out_channels, kernel_size=3, padding=1))
        self.conv2 = nn.utils.spectral_norm(nn.Conv2d(out_channels, out_channels, kernel_size=3, padding=1))

        self.activation = nn.ReLU()
        self.use_preactivation = use_preactivation  # apply preactivation in all except first dblock

        self.downsample = downsample    # downsample occurs in all except last dblock
        if downsample:
            self.downsample_fn = nn.AvgPool2d(2)
        self.mixin = (in_channels != out_channels) or downsample
        if self.mixin:
            self.conv_mixin = nn.utils.spectral_norm(nn.Conv2d(in_channels, out_channels, kernel_size=1, padding=0))

    def _residual(self, x):
        if self.use_preactivation:
            if self.mixin:
                x = self.conv_mixin(x)
            if self.downsample:
                x = self.downsample_fn(x)
        else:
            if self.downsample:
                x = self.downsample_fn(x)
            if self.mixin:
                x = self.conv_mixin(x)
        return x

    def forward(self, x):
        # Apply preactivation if applicable
        if self.use_preactivation:
            h = F.relu(x)
        else:
            h = x

        h = self.conv1(h)
        h = self.activation(h)
        if self.downsample:
            h = self.downsample_fn(h)

        return h + self._residual(x)
    
class Discriminator(nn.Module):
    '''
    base_channels: the number of base channels
    n_classes: the number of image classes
    '''

    def __init__(self, base_channels=96, n_classes=5):
        super().__init__()

        # For adding class-conditional evidence
        self.shared_emb = nn.utils.spectral_norm(nn.Embedding(n_classes, 16 * base_channels))

        self.d_blocks = nn.Sequential(
            DResidualBlock(3, base_channels, downsample=True, use_preactivation=False),
            AttentionBlock(base_channels),

            DResidualBlock(base_channels, 2 * base_channels, downsample=True, use_preactivation=True),
            AttentionBlock(2 * base_channels),

            DResidualBlock(2 * base_channels, 4 * base_channels, downsample=True, use_preactivation=True),
            AttentionBlock(4 * base_channels),

            DResidualBlock(4 * base_channels, 8 * base_channels, downsample=True, use_preactivation=True),
            AttentionBlock(8 * base_channels),

            DResidualBlock(8 * base_channels, 16 * base_channels, downsample=True, use_preactivation=True),
            AttentionBlock(16 * base_channels),

            DResidualBlock(16 * base_channels, 16 * base_channels, downsample=False, use_preactivation=True),
            AttentionBlock(16 * base_channels),

            nn.ReLU(inplace=True),
        )
        self.proj_o = nn.utils.spectral_norm(nn.Linear(16 * base_channels, 1))

    def forward(self, x, y=None):
        h = self.d_blocks(x)
        h = torch.sum(h, dim=[2, 3])

        # Class-unconditional output
        uncond_out = self.proj_o(h)
        if y is None:
            return uncond_out

        # Class-conditional output
        cond_out = torch.sum(self.shared_emb(y) * h, dim=1, keepdim=True)
        return uncond_out + cond_out
    

# INITIALIZE MODELS
gen = Generator(base_channels=base_channels, bottom_width=4, z_dim=z_dim, shared_dim=shared_dim, n_classes=n_classes).to(device)
disc = Discriminator(base_channels=base_channels, n_classes=n_classes).to(device)

# Initialize weights orthogonally
for module in gen.modules():
    if (isinstance(module, nn.Conv2d) or isinstance(module, nn.Linear) or isinstance(module, nn.Embedding)):
        nn.init.orthogonal_(module.weight)
for module in disc.modules():
    if (isinstance(module, nn.Conv2d) or isinstance(module, nn.Linear) or isinstance(module, nn.Embedding)):
        nn.init.orthogonal_(module.weight)

# Initialize optimizers
opt_G = torch.optim.Adam(gen.parameters(), lr=1e-4, betas=(0.0, 0.999), eps=1e-6)
opt_D = torch.optim.Adam(disc.parameters(), lr=4e-4, betas=(0.0, 0.999), eps=1e-6)



# DATASET LOADER
dataloader = DataLoader(dataset=dataset,
                         batch_size=batch_size,
                         shuffle=True)

# TRAINING
generator_losses = []
discriminator_losses = []
for epoch in range(n_epochs):
    # i is the batch number
    for i, (real, labels) in enumerate(tqdm(dataloader)):
        
        # batch are the reals
        real = real.to(device)
        labels = labels.to(device)

        # ====================
        # UPDATE DISCRIMINATOR
        # ====================
        
        # zero out the gradients
        opt_D.zero_grad()
        
        # get the noise
        z = get_noise(len(real), z_dim, device=device)
        
        # generate batch of labels (y)
        y = torch.arange(start=0, end=len(real), device=device).long() 
        
        # get class embeddings (y_emb) from generator
        y_emb = gen.shared_emb(y)
        
        # get fake images from z and y_emb
        fake = gen(z, y_emb).detach()
        
        # get the discriminator predictions
        disc_fake_pred = disc(fake, y)
        disc_real_pred = disc(real, labels)
         
        # compute the disc losses
        disc_fake_loss = criterion(disc_fake_pred, torch.zeros_like(disc_fake_pred))
        disc_real_loss = criterion(disc_real_pred, torch.ones_like(disc_real_pred))
        
        # compute the avg loss
        disc_loss = (disc_fake_loss + disc_real_loss) / 2
        
        # backpropagation to compute gradients for all layers
        disc_loss.backward(retain_graph=True)
        
        # update weights for this batch
        opt_D.step() 
        
        # ================
        # UPDATE GENERATOR 
        # ================
 
        
        # zero out the gradients
        opt_G.zero_grad()

        # get the noise
        z = get_noise(len(real), z_dim, device=device)
        
        # generate batch of labels (y)
        y = torch.arange(start=0, end=len(real), device=device).long() 
        
        # get class embeddings (y_emb) from generator
        y_emb = gen.shared_emb(y)

        # get fake images from z and y_emb
        fake = gen(z, y_emb)
        
        # get the predictions for the fakes
        disc_fake_pred = disc(fake, y)
        
        # gen wants the disc to think its fakes are real (pred 1)
        gen_loss = criterion(disc_fake_pred, torch.ones_like(disc_fake_pred))
        
        # backpropagation to compute gradients for all layers
        gen_loss.backward()
        
        # update weights for this batch
        opt_G.step()
        

        # current epoch nr * iter per epochs + iterations in current loop 
        batches_per_epoch = len(dataloader)
        total_finished_batches = (epoch + 1) * (batches_per_epoch) + (i + 1)
        
        # save loss in list after each epoch
        if total_finished_batches % batches_per_epoch == 0:
            discriminator_losses += [disc_loss.item()]
            generator_losses += [gen_loss.item()]
        
        # only save gen/loss progress images every save_nr epochs (if 5000 epochs)
        if total_finished_batches % (batches_per_epoch * save_nr) == 0:
            save_mixed_images(25, epoch, gen_imgs_path)
            plot_losses(generator_losses, discriminator_losses, loss_plots_path)
        if (epoch + 1) > checkpoint_start and total_finished_batches % (batches_per_epoch * checkpoint_nr) == 0:
            torch.save({
                'G_state_dict': gen.state_dict(),
                'D_state_dict': disc.state_dict(),
                'opt_G': opt_G.state_dict(),
                'opt_D': opt_D.state_dict()
            }, checkpoints_path + '/chkpt_epoch%d.pt' % (epoch + 2))

    # print the numbers after each epoch
    print('[Epoch %d/%d] [D loss: %f] [G loss: %f]' % 
    (epoch+1,  n_epochs,
    discriminator_losses[-1], generator_losses[-1]))

